#ifndef PARAMETERLIST_H
#define PARAMETERLIST_H

#include <QMap>
#include <QString>

namespace Utils
{

   class ParameterList
   {
   public:
       ParameterList();

       const QString toString() const;
   private:
       QMap<QString, QString> KeyValuePairs;
   };

}

#endif // PARAMETERLIST_H

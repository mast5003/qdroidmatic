#include "ParameterList.h"

using namespace Utils;

ParameterList::ParameterList()
{

}

const QString ParameterList::toString() const
{
   QString result, key;
   foreach(key, KeyValuePairs.keys())
   {
      result += key + "=" + KeyValuePairs.value(key, "");
   }
   return result;
}

#include "RequestService.h"
#include "Requests/Request.h"

#include <QEvent>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

using namespace Requests;

RequestService::RequestService(QObject *parent)
   : QQmlComponent(parent)
   , NetMan(this)
   , CurrentReply(nullptr)
   , RequestData("")
   , CurrentRequest(nullptr)
{
}

RequestService::~RequestService()
{
   delete CurrentReply;
}

bool RequestService::event(QEvent *e)
{
   if (e->type() == QEvent::ThreadChange)
   {
      //NetMan.moveToThread(NetMan.parent()->thread());
   }
   return QObject::event(e);
}

void RequestService::RunRequest(Request *req)
{
   Request::Types type = req->getType();
   (void) type;
   QNetworkRequest qRequest(req->toUrl());
   CurrentReply = NetMan.get(qRequest);
   connect(CurrentReply, &QNetworkReply::finished, this, &RequestService::OnRequestReturns);
   connect(CurrentReply, &QIODevice::readyRead, this, &RequestService::OnRequestData);
   CurrentRequest = req;
}

void RequestService::OnRequestReturns()
{
   emit RespondRequest(CurrentRequest, RequestData);
   RequestData = "";
}

void RequestService::OnRequestData()
{
   if (CurrentReply)
   {
      RequestData += CurrentReply->readAll();
   }
}

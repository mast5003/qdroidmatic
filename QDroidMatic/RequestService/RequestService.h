#ifndef REQUESTSERVICE_H
#define REQUESTSERVICE_H

#include <QQmlComponent>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include "Requests/Request.h"

class RequestService : public QQmlComponent
{
   Q_OBJECT

public:
   explicit RequestService(QObject *parent = nullptr);
   virtual ~RequestService() override;

   virtual bool event(QEvent*) override;

signals:
   void RespondRequest(Requests::Request*, QString);

public slots:
   void RunRequest(Requests::Request *req);

private slots:
   void OnRequestReturns();
   void OnRequestData();

private:

   QNetworkAccessManager NetMan;
   QNetworkReply *CurrentReply;
   QString RequestData;
   Requests::Request* CurrentRequest;
};

#endif // REQUESTSERVICE_H

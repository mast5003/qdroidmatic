import QtQuick 2.9
import QtQuick.Controls 2.2
import com.QDroidMatic.Model 1.0

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 1440
    height: 2650
    title: qsTr("Scroll")


    Column {
        id: column
        width: parent.width
        height: parent.height
        anchors.top: parent.top
        anchors.topMargin: 0
        spacing: 2

        Label {
            id: xmlapiVersionLabel
            width: parent.width
            text: qsTr("Label")
            anchors.top: parent.top
            anchors.topMargin: 0
            objectName: "xmlapiVersionLabel"
        }

        ComboBox {
            id: comboBox
            objectName: "comboBoxMenu"
            width: parent.width
            anchors.top: xmlapiVersionLabel.bottom
            anchors.topMargin: 9
        }

        ScrollView {
            width: parent.width
            anchors.top: comboBox.bottom
            anchors.topMargin: 0

            ListView {
                width: parent.width
                objectName: "menuListView"
                model: GuiControlListViewMenuModel
            }
        }

    }

}

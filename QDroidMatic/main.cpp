#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <RequestService/RequestService.h>
#include <QThread>
#include "GuiControl.h"
#include "GuiControlListViewMenuModel.h"

int main(int argc, char *argv[])
{
    QThread *ServiceThread = new QThread();
    ServiceThread->start();

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    qmlRegisterType<GuiControlListViewMenuModel>("com.QDroidMatic.Model", 1, 0, "GuiControlListViewMenuModel");

    QQmlApplicationEngine engine;
    RequestService *service = new RequestService(nullptr);
    GuiControl *guiControl = new GuiControl(engine, *service);
    service->moveToThread(ServiceThread);
    bool test = ServiceThread->isRunning();
    (void) test;

    bool connected = QObject::connect(service, SIGNAL(RespondRequest(Requests::Request*, QString)), guiControl, SLOT(OnRespondRequest(Requests::Request*, QString)));

    //service->RunRequest(Requests::Request());
    const QUrl mainQml(QStringLiteral("qrc:/main.qml"));

    connected = QObject::connect(&engine, SIGNAL(objectCreated(QObject*, const QUrl)), guiControl, SLOT(OnApplictionLoaded()));

    // Catch the objectCreated signal, so that we can determine if the root component was loaded
    // successfully. If not, then the object created from it will be null. The root component may
    // get loaded asynchronously.
    const QMetaObject::Connection connection = QObject::connect(
                &engine, &QQmlApplicationEngine::objectCreated,
                &app, [&](QObject *object, const QUrl &url) {
            if (url != mainQml)
            return;

            if (!object)
            app.exit(-1);
            else
            QObject::disconnect(connection);
}, Qt::QueuedConnection);

    engine.load(mainQml);
    return app.exec();
}

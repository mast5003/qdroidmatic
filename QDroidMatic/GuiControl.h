#ifndef GUICONTROL_H
#define GUICONTROL_H

#include <QObject>
#include "Requests/Request.h"

class QQmlApplicationEngine;

class GuiControl : public QObject
{
    Q_OBJECT

public:
   GuiControl(QQmlApplicationEngine& engine, QObject& requestService);

public slots:
   void OnRespondRequest(Requests::Request *req, QString result);
   void OnApplictionLoaded();
   void OnComboBoxMenuAccepted();
   void OnListViewClicked();

private:
   void SetUpMenuLabel(const QString &result);
   void SetUpComboBoxMenu();
   void UpdateMenuListView(const QString& result, const QString &element, const QString &attr);

   QObject* GetQmlObjectByName(const QString &objectName);

   QQmlApplicationEngine& Engine;
   QObject& RequestService;
};

#endif // GUICONTROL_H

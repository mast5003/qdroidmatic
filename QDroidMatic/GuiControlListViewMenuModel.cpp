#include "GuiControlListViewMenuModel.h"

GuiControlListViewMenuModel::GuiControlListViewMenuModel()
{

}

void GuiControlListViewMenuModel::registerTypes(const char *uri)
{
   qmlRegisterType<GuiControlListViewMenuModel>(uri, 1, 0, "GuiControlListViewMenuModel");
}

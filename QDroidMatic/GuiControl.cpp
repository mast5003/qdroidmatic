#include "GuiControl.h"

#include <QtXml/QDomDocument>
#include <QQmlApplicationEngine>
#include <Requests/RequestVersion.h>
#include <Requests/RequestDeviceList.h>
#include <Requests/RequestFunctionList.h>
#include <Requests/RequestRoomList.h>
#include <Requests/RequestProgramList.h>
#include <Requests/RequestVariablesList.h>
#include <Requests/RequestProtocol.h>
#include <Requests/RequestFavoriteList.h>

#include <QQmlContext>

Q_DECLARE_METATYPE(Requests::Request);
Q_DECLARE_METATYPE(Requests::Request*);

GuiControl::GuiControl(QQmlApplicationEngine &engine, QObject &requestService)
   : QObject(nullptr)
   , Engine(engine)
   , RequestService(requestService)
{
   qRegisterMetaType<Requests::Request>("Requests::Request");
   qRegisterMetaType<Requests::Request*>("Requests::Request*");
}

// Fixme: double check if instead of switch case the slot could be overloaded
void GuiControl::OnRespondRequest(Requests::Request *req, QString result)
{
   switch (req->getType())
   {
      case(Requests::Request::Types::VERSION):
      {
         SetUpMenuLabel(result);
         SetUpComboBoxMenu();
         break;
      }
      case(Requests::Request::Types::DEVICE_LIST):
      {
         UpdateMenuListView(result, "device", "name");
         break;
      }
      case(Requests::Request::Types::FUNCTION_LIST):
      {
         UpdateMenuListView(result, "function", "name");
         break;
      }
      case(Requests::Request::Types::FAVORITE_LIST):
      {
         UpdateMenuListView(result, "favorite", "name");
         break;
      }
      case(Requests::Request::Types::MASTER_VALUE):
      case(Requests::Request::Types::MASTER_VALUE_CHANGE):
      case(Requests::Request::Types::PROGRAM_LIST):
      case(Requests::Request::Types::PROGRAM_ACTIONS):
      case(Requests::Request::Types::PROTOCOL):
      case(Requests::Request::Types::RUNPROGRAM):
      case(Requests::Request::Types::ROOM_LIST):
      {
         UpdateMenuListView(result, "room", "name");
         break;
      }
      case(Requests::Request::Types::RSSI_LIST):
      case(Requests::Request::Types::SCRIPT_ERRORS):
      case(Requests::Request::Types::STATE):
      case(Requests::Request::Types::STATE_LIST):
      case(Requests::Request::Types::STATE_CHANGE):
      case(Requests::Request::Types::SYSTEM_NOTOFICATION):
      case(Requests::Request::Types::SYSTEM_NOTIFICATION_CLEAR):
      case(Requests::Request::Types::SYS_VAR):
      case(Requests::Request::Types::SYS_VAR_LIST):
      {
         UpdateMenuListView(result, "systemVariable", "name");
         break;
      }
      case(Requests::Request::Types::INVALID):
      {
         QList<QObject *> list = Engine.rootObjects();
         QObject* root;
         foreach (root, list)
         {
            QObject* label = root->findChild<QObject*>("xmlapiVersionLabel");
            if (label)
            {
              label->setProperty("text", "INVALID Request");
            }
         }
         break;
      }
      default:
      {
         QList<QObject *> list = Engine.rootObjects();
         QObject* root;
         foreach (root, list)
         {
            QObject* label = root->findChild<QObject*>("xmlapiVersionLabel");
            if (label)
            {
              label->setProperty("text", "undefined Request response");
            }
         }
      }
   }
}

void GuiControl::OnApplictionLoaded()
{
   QObject* comboBoxMenu = GetQmlObjectByName("comboBoxMenu");
   QObject::connect(comboBoxMenu, SIGNAL(accepted()), this, SLOT(OnComboBoxMenuAccepted()), Qt::QueuedConnection);
   QObject::connect(comboBoxMenu, SIGNAL(activated(int)), this, SLOT(OnComboBoxMenuAccepted()), Qt::QueuedConnection);

   QObject* listView = GetQmlObjectByName("menuListView");
   QObject::connect(listView, SIGNAL(clicked()), this, SLOT(OnListViewClicked()), Qt::QueuedConnection);

   Requests::RequestVersion* request = new Requests::RequestVersion();
   QMetaObject::invokeMethod(&RequestService, "RunRequest", Qt::QueuedConnection, Q_ARG(Requests::Request*, request));
}

void GuiControl::OnComboBoxMenuAccepted()
{
   QObject* comboBoxMenu = GetQmlObjectByName("comboBoxMenu");
   QObject* label = GetQmlObjectByName("xmlapiVersionLabel");

   if (label && comboBoxMenu)
   {
      const QString &currentValue = comboBoxMenu->property("currentText").toString();
      if ("Devices" == currentValue)
      {
         Requests::RequestDeviceList *request = new Requests::RequestDeviceList();
         QMetaObject::invokeMethod(&RequestService, "RunRequest", Qt::QueuedConnection, Q_ARG(Requests::Request*, request));
      }
      else if ("Functions" == currentValue)
      {
         Requests::RequestFunctionList *request = new Requests::RequestFunctionList();
         QMetaObject::invokeMethod(&RequestService, "RunRequest", Qt::QueuedConnection, Q_ARG(Requests::Request*, request));
      }
      else if ("Favorites" == currentValue)
      {
         Requests::RequestFavoriteList *request = new Requests::RequestFavoriteList();
         QMetaObject::invokeMethod(&RequestService, "RunRequest", Qt::QueuedConnection, Q_ARG(Requests::Request*, request));
      }
      else if ("Programs" == currentValue)
      {
         Requests::RequestProgramList *request = new Requests::RequestProgramList();
         QMetaObject::invokeMethod(&RequestService, "RunRequest", Qt::QueuedConnection, Q_ARG(Requests::Request*, request));
      }
      else if ("Protocol" == currentValue)
      {
         Requests::RequestProtocol *request = new Requests::RequestProtocol();
         QMetaObject::invokeMethod(&RequestService, "RunRequest", Qt::QueuedConnection, Q_ARG(Requests::Request*, request));
      }
      else if ("Rooms" == currentValue)
      {
         Requests::RequestRoomList *request = new Requests::RequestRoomList();
         QMetaObject::invokeMethod(&RequestService, "RunRequest", Qt::QueuedConnection, Q_ARG(Requests::Request*, request));
      }
      else if ("Variables" == currentValue)
      {
         Requests::RequestVariablesList *request = new Requests::RequestVariablesList();
         QMetaObject::invokeMethod(&RequestService, "RunRequest", Qt::QueuedConnection, Q_ARG(Requests::Request*, request));
      }
   }
}

void GuiControl::OnListViewClicked()
{
   QObject* comboBoxMenu = GetQmlObjectByName("comboBoxMenu");
   if (comboBoxMenu)
   {
      const QString &currentValue = comboBoxMenu->property("currentText").toString();
      if ("Devices" == currentValue)
      {
         QObject* listView = GetQmlObjectByName("listView");
         if (listView)
         {
            int currentIndex = listView->property("currentIndex").toInt();
            if(-1 != currentIndex)
            {
               QQmlContext *ctxt = Engine.rootContext();
               QStringList model = ctxt->contextProperty("menuListViewModel").toStringList();
               QString DeviceName = model.at(currentIndex);
               (void) DeviceName;
            }
         }
      }
   }
}

void GuiControl::SetUpMenuLabel(const QString &result)
{
   QString version;
   QDomDocument doc;
   doc.setContent(result);
   QDomNodeList list = doc.elementsByTagName("version");
   for(int i = 0; i < list.length(); ++i)
   {
      QDomNode node = list.item(i);
      version = node.nodeValue();
   }

   QObject* label = GetQmlObjectByName("xmlapiVersionLabel");
   if (label)
   {
     label->setProperty("text", version);
   }
}

void GuiControl::SetUpComboBoxMenu()
{
   QList<QObject *> list = Engine.rootObjects();
   QObject* root;
   foreach (root, list)
   {
      QObject* label = root->findChild<QObject*>("comboBoxMenu");
      if (label)
      {
        QStringList requestList;
        requestList.append("Devices");
        requestList.append("Functions");
        requestList.append("Favorites");
        requestList.append("Programs");
        requestList.append("Protocol");
        requestList.append("Rooms");
        requestList.append("Devices");
        requestList.append("Variables");
        label->setProperty("model", requestList);
        break;
      }
   }
}

void GuiControl::UpdateMenuListView(const QString &result, const QString& element, const QString &attr)
{
   QDomDocument doc;
   doc.setContent(result);
   QStringList dataList;
   QDomNodeList list = doc.elementsByTagName(element);
   for(int i = 0; i < list.length(); ++i)
   {
      QDomNamedNodeMap map = list.item(i).attributes();
      QDomNode attrName = map.namedItem(attr);
      dataList.append(attrName.nodeValue());
   }

   QQmlContext *ctxt = Engine.rootContext();
   ctxt->setContextProperty("menuListViewModel", QVariant::fromValue(dataList));
}

QObject *GuiControl::GetQmlObjectByName(const QString& objectName)
{
   QObject* object = nullptr;
   QList<QObject *> list = Engine.rootObjects();
   QObject* root;
   foreach (root, list)
   {
      object = root->findChild<QObject*>(objectName);
   }
   return object;
}

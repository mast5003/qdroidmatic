#ifndef REQUEST_H
#define REQUEST_H

#include <QString>
#include <QUrl>
#include <Utils/ParameterList.h>

namespace Requests
{

    // http://<CCU-IP>/addons/xmlapi/statechange.cgi?ise_id=12345&new_value=0.20
    class Request
    {
    public:
        Request();
        virtual ~Request();

        enum class Types
        {
           INVALID,
           DEVICE_LIST,
           FUNCTION_LIST,
           FAVORITE_LIST,
           MASTER_VALUE,
           MASTER_VALUE_CHANGE,
           PROGRAM_LIST,
           PROGRAM_ACTIONS,
           PROTOCOL,
           RUNPROGRAM,
           ROOM_LIST,
           RSSI_LIST,
           SCRIPT_ERRORS,
           STATE,
           STATE_LIST,
           STATE_CHANGE,
           SYSTEM_NOTOFICATION,
           SYSTEM_NOTIFICATION_CLEAR,
           SYS_VAR,
           SYS_VAR_LIST,
           VERSION
        };

        const QString toUrlEncodedRequestString();
        QUrl toUrl() const;

        virtual Types getType() const;


    protected:
        QString CgiScript;
        Utils::ParameterList ParameterList;
    private:
        QString Protocol;
        QString Server;
        QString ApiPath;
    };

}

#endif // REQUEST_H

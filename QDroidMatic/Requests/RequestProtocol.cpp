#include "RequestProtocol.h"

using namespace Requests;

RequestProtocol::RequestProtocol()
{
   CgiScript = "protocol.cgi";
}

Requests::RequestProtocol::~RequestProtocol()
{

}

Requests::Request::Types Requests::RequestProtocol::getType() const
{
   return Types::PROTOCOL;
}

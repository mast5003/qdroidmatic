#include "RequestProgramList.h"

using namespace Requests;

RequestProgramList::RequestProgramList()
{
   CgiScript = "programlist.cgi";
}

Requests::RequestProgramList::~RequestProgramList()
{

}

Requests::Request::Types Requests::RequestProgramList::getType() const
{
return Types::PROGRAM_LIST;
}

#include "RequestFavoriteList.h"

using namespace Requests;

RequestFavoriteList::RequestFavoriteList()
{
    CgiScript = "favoritelist.cgi";
}

RequestFavoriteList::~RequestFavoriteList()
{

}

Request::Types RequestFavoriteList::getType() const
{
   return Types::FAVORITE_LIST;
}

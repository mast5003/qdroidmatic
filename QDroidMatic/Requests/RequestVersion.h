#ifndef REQUESTVERSION_H
#define REQUESTVERSION_H

#include <Requests/Request.h>

namespace Requests
{
   class RequestVersion : public Request
   {
   public:
      RequestVersion();
      virtual ~RequestVersion() override;
      virtual Types getType() const override;
   };
}
#endif // REQUESTVERSION_H

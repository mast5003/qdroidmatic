#ifndef REQUESTPROGRAMLIST_H
#define REQUESTPROGRAMLIST_H

#include <Requests/Request.h>

namespace Requests
{
   class RequestProgramList : public Request
   {
   public:
      RequestProgramList();
      virtual ~RequestProgramList() override;

      virtual Types getType() const override;
   };
}
#endif // REQUESTPROGRAMLIST_H

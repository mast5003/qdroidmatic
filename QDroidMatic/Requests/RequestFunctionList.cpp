#include "RequestFunctionList.h"

using namespace Requests;

RequestFunctionList::RequestFunctionList()
{
    CgiScript = "functionlist.cgi";
}

Requests::RequestFunctionList::~RequestFunctionList()
{

}

Requests::Request::Types Requests::RequestFunctionList::getType() const
{
    return Request::Types::FUNCTION_LIST;
}

#ifndef REQUESTVARIABLESLIST_H
#define REQUESTVARIABLESLIST_H

#include <Requests/Request.h>

namespace Requests
{

    class RequestVariablesList : public Request
    {
    public:
       RequestVariablesList();
       virtual ~RequestVariablesList() override;

       virtual Request::Types getType() const override;
    };

}
#endif // REQUESTVARIABLESLIST_H

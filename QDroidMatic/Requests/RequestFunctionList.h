#ifndef REQUESTFUNCTIONLIST_H
#define REQUESTFUNCTIONLIST_H

#include <Requests/Request.h>

namespace Requests
{

    class RequestFunctionList : public Request
    {
    public:
       RequestFunctionList();
       virtual ~RequestFunctionList() override;

       virtual Request::Types getType() const override;
    };

}
#endif // REQUESTFUNCTIONLIST_H

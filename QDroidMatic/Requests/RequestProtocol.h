#ifndef REQUESTPROTOCOL_H
#define REQUESTPROTOCOL_H

#include <Requests/Request.h>

namespace Requests
{

   class RequestProtocol : public Request
   {
   public:
      RequestProtocol();
      virtual ~RequestProtocol() override;

      virtual Types getType() const override;
   };

}
#endif // REQUESTPROTOCOL_H

#ifndef REQUESTDEVICELIST_H
#define REQUESTDEVICELIST_H

#include <Requests/Request.h>

namespace Requests
{

   class RequestDeviceList : public Request
   {
   public:
      RequestDeviceList();
      virtual ~RequestDeviceList() override;

      virtual Types getType() const override;
   };

}
#endif // REQUESTDEVICELIST_H

#include "RequestDeviceList.h"

using namespace Requests;

RequestDeviceList::RequestDeviceList()
: Request()
{
   CgiScript = "devicelist.cgi";
}

Requests::RequestDeviceList::~RequestDeviceList()
{

}

Request::Types RequestDeviceList::getType() const
{
   return Request::Types::DEVICE_LIST;
}

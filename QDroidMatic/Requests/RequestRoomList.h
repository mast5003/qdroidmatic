#ifndef REQUESTROOMLIST_H
#define REQUESTROOMLIST_H

#include <Requests/Request.h>

namespace Requests
{
    class RequestRoomList : public Request
    {
    public:
       RequestRoomList();
       virtual ~RequestRoomList() override;

       virtual Request::Types getType() const override;
    };
}
#endif // REQUESTROOMLIST_H

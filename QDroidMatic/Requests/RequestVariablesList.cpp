#include "RequestVariablesList.h"

using namespace Requests;

RequestVariablesList::RequestVariablesList()
{
    CgiScript = "sysvarlist.cgi";
}

RequestVariablesList::~RequestVariablesList()
{

}

Request::Types RequestVariablesList::getType() const
{
  return Types::SYS_VAR_LIST;
}

#include "RequestRoomList.h"

using namespace Requests;

RequestRoomList::RequestRoomList()
{
    CgiScript = "roomlist.cgi";
}

Requests::RequestRoomList::~RequestRoomList()
{

}

Requests::Request::Types Requests::RequestRoomList::getType() const
{
   return Requests::Request::Types::ROOM_LIST;
}

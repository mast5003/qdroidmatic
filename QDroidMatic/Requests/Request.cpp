#include "Request.h"

#include <QUrl>

using namespace Requests;

Request::Request()
   : CgiScript("version.cgi")
   , ParameterList()
   , Protocol("http")
   , Server("homematic-raspi")
   , ApiPath("addons/xmlapi")
{
}

Request::~Request()
{

}

const QString Request::toUrlEncodedRequestString()
{
   return Protocol + Server + ApiPath + CgiScript + ParameterList.toString();
}

QUrl Request::toUrl() const
{
   return QUrl(Protocol + "://" + Server + "/" + ApiPath + "/" + CgiScript + "?" + ParameterList.toString());
}

Request::Types Request::getType() const
{
   return Types::INVALID;
}

#ifndef REQUESTFAVORITELIST_H
#define REQUESTFAVORITELIST_H

#include <Requests/Request.h>

namespace Requests
{

    class RequestFavoriteList : public Request
    {
    public:
       RequestFavoriteList();
       virtual ~RequestFavoriteList() override;

       virtual Types getType() const override;

    };

}
#endif // REQUESTFAVORITELIST_H

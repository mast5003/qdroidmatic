#ifndef GUICONTROLLISTVIEWMENUMODEL_H
#define GUICONTROLLISTVIEWMENUMODEL_H

#include <QtQml>

class GuiControlListViewMenuModel : public QQmlExtensionPlugin
{
    Q_OBJECT
    //Q_PLUGIN_METADATA(IID "org.qt-project.QmlExtension.GuiControlListViewMenuModel" FILE "GuiControlListViewMenuModel.json")

public:
   GuiControlListViewMenuModel();
   virtual ~GuiControlListViewMenuModel() = default;
   void registerTypes(const char *uri);
};

#endif // GUICONTROLLISTVIEWMENUMODEL_H
